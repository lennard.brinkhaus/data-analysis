package git.rwth_aachen.de.data_analysis;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.highgui.HighGui;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.nio.file.Path;
import java.util.Objects;

public class Main {
  static final int xSize = 774;
  static final int ySize = 1032;

  public static boolean DEBUG = false;

  public static void main(String[] args) {
    if (args.length < 2) {
      System.err.println("You need to give the Empty Dartboard as first Parameter and the filled Dartboard as second Parameter");
      return;
    }

    String path  = args[0];
    String dartPath = args[1];

    if (args.length > 2) {
      String debug = args[2];
      Main.DEBUG = Objects.equals(debug, "--debug");
    }

    File f = new File(path);
    if (!f.exists() ||f.isDirectory()) {
      System.err.println("Empty Dartboard does not exists or is an Directory");
      return;
    }

    f = new File(dartPath);
    if (!f.exists() ||f.isDirectory()) {
      System.err.println("Filled Dartboard does not exists or is an Directory");
      return;
    }

    nu.pattern.OpenCV.loadLocally();

    Mat view = Imgcodecs.imread(dartPath, Imgcodecs.IMREAD_COLOR);
    var x = view.cols();
    var y = view.rows();
    BoardDetection boardDetection = new BoardDetection(Path.of(path));

    try {
      boolean success = boardDetection.init();
      if (!success) {
        System.err.println("Problem while initial the Image");
        return;
      }
    } catch (UnsatisfiedLinkError err) {
      System.err.println("Couldn't load the image: '" + path + "'");
      return;
    }
    DartDetection dartdetection = new DartDetection();
    Point[] dartPositions = dartdetection.detectDarts(dartPath, path);

    int points = 0;

    for (Point dartPoint : dartPositions) {
      int currPoint = boardDetection.calculatePoints(dartPoint);
      System.out.println(String.format("[%.2f/%.2f] -> %d", dartPoint.x, dartPoint.y, currPoint));
      points += currPoint;

      Imgproc.circle(view, dartPoint, 10,new Scalar(255, 0, 255), -1);
    }

    System.out.println("All Points: " + points);
    Imgproc.circle(view, boardDetection.getCenter(), boardDetection.getRadius(), new Scalar(255, 0, 0));

    if (DEBUG) {
      Imgproc.resize(view, view,new Size(xSize, ySize) );
      HighGui.namedWindow("Detection Information", HighGui.WINDOW_AUTOSIZE);
      HighGui.imshow("Detection Information", view);
      HighGui.waitKey();
    }
  }
}
