package git.rwth_aachen.de.data_analysis;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;

import static git.rwth_aachen.de.data_analysis.Main.*;
import static org.opencv.core.Core.absdiff;
import static org.opencv.imgproc.Imgproc.*;

public class DartDetection {

  private static final int MAX_LOW_THRESHOLD = 150;
  private static final int RATIO = 3;
  private static final int KERNEL_SIZE = 3;

  /**
   * @param pictureWithDarts
   * @return Point Array with size 4, first value is center of Dartboard and the others are the Positions of the Darts
   * in coordinate system, where the the center has coordinates (0,0).
   */
  public Point[] detectDarts(String pictureWithDarts, String pictureWithoutDarts) {
    Point[] result = new Point[3];
    try {

      Mat dartboard = Imgcodecs
        .imread(pictureWithoutDarts, Imgcodecs.IMREAD_COLOR);
      Mat grayDartboard = Imgcodecs
        .imread(pictureWithoutDarts, Imgcodecs.IMREAD_GRAYSCALE);

      Mat withDarts = Imgcodecs
        .imread(pictureWithDarts, Imgcodecs.IMREAD_COLOR);
      Mat grayWithDarts = Imgcodecs
        .imread(pictureWithDarts, Imgcodecs.IMREAD_GRAYSCALE);

      var x = withDarts.cols();
      var y = withDarts.rows();

      resize(dartboard, dartboard, new Size(xSize, ySize));
      resize(withDarts, withDarts, new Size(xSize, ySize));
      resize(grayWithDarts, grayWithDarts, new Size(xSize, ySize));
      resize(grayDartboard, grayDartboard, new Size(xSize, ySize));

      Mat edgesDartboard = edgeDetection(dartboard);
      Mat circles = circleDetection(edgesDartboard, 1);
//      Mat circles5 = circleDetection(edgeDetection(dartboard), 2);
//      Mat circles3 = circleDetection(edgeDetection(grayDartboard), 1);
//      Mat circles4 = circleDetection(edgeDetection(grayDartboard), 2);
//      Mat circles8 = circleDetection(edgeDetection(dartboard), 3);
//      Mat circles9 = circleDetection(edgeDetection(grayDartboard), 3);
      Point centerOfDartboard = getAverageCenterOfAll(new Mat[]{circles
//        ,circles3,circles4,circles5,circles8,circles9
      });

      double[] distanceInBullEyeWithoutDarts = getDistanceToNearEdges(centerOfDartboard, edgesDartboard.clone());

      //paintCircels(dartboard, circles, false);
//      paintCircels(dartboard, circles3, false);
//      paintCircels(dartboard, circles4, false);
//      paintCircels(dartboard, circles5, false);
//      paintCircels(dartboard, circles8, false);
//      Imgproc.circle(dartboard, centerOfDartboard, 3, new Scalar(255, 255, 0), -1);
//      HighGui.imshow("test image ", dartboard);

      Mat edgesDartboardWithDarts = edgeDetection(withDarts);
      Mat circles2 = circleDetection(edgeDetection(grayWithDarts), 2);
      Mat circles1 = circleDetection(edgesDartboardWithDarts, 2);
//      Mat circles6 = circleDetection(edgeDetection(grayWithDarts), 1);
//      Mat circles7 = circleDetection(edgeDetection(withDarts), 1);
//      Mat circles10 = circleDetection(edgeDetection(grayWithDarts), 3);
//      Mat circles11 = circleDetection(edgeDetection(withDarts), 3);

      Point centerOfDartboardWithDarts = getAverageCenterOfAll(new Mat[]{circles1,
//        circles6,circles7,circles10,circles11,
        circles2});

      double[] distanceInBullEyeWithDarts = getDistanceToNearEdges(centerOfDartboardWithDarts, edgesDartboardWithDarts.clone());
      ArrayList<Point> previousTrys = new ArrayList<>();

      //FIXME funktioniert grundsätzlich, wenn z.B.das Referenzbild genau gerade nach oben versetzt ist
      //      (siehe dartboard-johannes2-ref). Leider funktioniert es (bisher) nicht bei beliebiger Versetzung.
      while (!Arrays.equals(distanceInBullEyeWithDarts, distanceInBullEyeWithoutDarts)) {
        previousTrys.add(centerOfDartboard.clone());
        boolean withDartsIsInUpperSection = distanceInBullEyeWithDarts[2] <= distanceInBullEyeWithDarts[3];
        boolean withoutDartsIsInUpperSection = distanceInBullEyeWithoutDarts[2] <= distanceInBullEyeWithoutDarts[3];
        if (withDartsIsInUpperSection && withoutDartsIsInUpperSection) {
          //points are in upper section
          if (distanceInBullEyeWithDarts[0] + distanceInBullEyeWithDarts[1] < distanceInBullEyeWithoutDarts[0] + distanceInBullEyeWithoutDarts[1]) {
            //pointWithoutDarts must higher
            centerOfDartboard.set(new double[]{centerOfDartboard.x, (centerOfDartboard.y - 1)});
          } else if (distanceInBullEyeWithDarts[0] + distanceInBullEyeWithDarts[1] > distanceInBullEyeWithoutDarts[0] + distanceInBullEyeWithoutDarts[1]) {
            //pointWithoutDarts must downer
            centerOfDartboard.set(new double[]{centerOfDartboard.x, (centerOfDartboard.y + 1)});
          } else {
            if (distanceInBullEyeWithDarts[0] < distanceInBullEyeWithoutDarts[0]) {
              //pointWithoutDarts must nearer to left edge
              centerOfDartboard.set(new double[]{centerOfDartboard.x - 1, (centerOfDartboard.y)});
            } else if (distanceInBullEyeWithDarts[1] < distanceInBullEyeWithoutDarts[1]) {
              //pointWithoutDarts must nearer to right edge
              centerOfDartboard.set(new double[]{centerOfDartboard.x + 1, (centerOfDartboard.y)});
            } else {
              if (distanceInBullEyeWithDarts[2] != distanceInBullEyeWithoutDarts[2]) {
                double shift = (distanceInBullEyeWithoutDarts[2] - distanceInBullEyeWithDarts[2]);
                centerOfDartboard.set(new double[]{centerOfDartboard.x, (centerOfDartboard.y + shift)});
              } else if (distanceInBullEyeWithDarts[3] != distanceInBullEyeWithoutDarts[3]) {
                double shift = (distanceInBullEyeWithoutDarts[3] - distanceInBullEyeWithDarts[3]);
                centerOfDartboard.set(new double[]{centerOfDartboard.x, (centerOfDartboard.y + shift)});
              }
            }
          }
        } else if (!withDartsIsInUpperSection && !withoutDartsIsInUpperSection) {
          //points are in under section
          if (distanceInBullEyeWithDarts[0] + distanceInBullEyeWithDarts[1] > distanceInBullEyeWithoutDarts[0] + distanceInBullEyeWithoutDarts[1]) {
            //pointWithoutDarts must higher
            centerOfDartboard.set(new double[]{centerOfDartboard.x, (centerOfDartboard.y - 1)});
          } else if (distanceInBullEyeWithDarts[0] + distanceInBullEyeWithDarts[1] < distanceInBullEyeWithoutDarts[0] + distanceInBullEyeWithoutDarts[1]) {
            //pointWithoutDarts must downer
            centerOfDartboard.set(new double[]{centerOfDartboard.x, (centerOfDartboard.y + 1)});
          } else {
            if (distanceInBullEyeWithDarts[0] < distanceInBullEyeWithoutDarts[0]) {
              //pointWithoutDarts must nearer to left edge
              centerOfDartboard.set(new double[]{centerOfDartboard.x - 1, (centerOfDartboard.y)});
            } else if (distanceInBullEyeWithDarts[1] < distanceInBullEyeWithoutDarts[1]) {
              //pointWithoutDarts must nearer to right edge
              centerOfDartboard.set(new double[]{centerOfDartboard.x + 1, (centerOfDartboard.y)});
            }
          }
        } else if (!withDartsIsInUpperSection) {
          //pointWithDarts is in under section and pointWithout in upper section
          double shift = (distanceInBullEyeWithoutDarts[3] - 1);
          if (shift <= 1) {
            shift++;
          }
          centerOfDartboard.set(new double[]{centerOfDartboard.x, (centerOfDartboard.y + shift)});
        } else {
          //pointWithDarts is in upper section and pointWithout in under section
          double shift = (distanceInBullEyeWithoutDarts[2] - 1);
          centerOfDartboard.set(new double[]{centerOfDartboard.x, (centerOfDartboard.y - shift)});
        }
        if (previousTrys.contains(centerOfDartboard)) {
          //FIXME Problem wenn ein Dart das Bullseye verdeckt oder drin liegt (siehe dartboardWithDarts4).
          // Dann funktioniert es nur bei identischer Bildgrundlage (dartboard-johannes-ref) durch gleichsetzten der Punkte.
          centerOfDartboard = centerOfDartboardWithDarts;
          break;
        }
        distanceInBullEyeWithoutDarts = getDistanceToNearEdges(centerOfDartboard, edgesDartboard.clone());
      }

//      System.out.println(""+centerOfDartboard + "  " + centerOfDartboardWithDarts);
//      paintCircels(withDarts, circles1, false);
 //     paintCircels(withDarts, circles2, false);
//      paintCircels(withDarts, circles6, false);
//      paintCircels(withDarts, circles7, false);
//      paintCircels(withDarts, circles10, false);
//      paintCircels(withDarts, circles11, false);
//      Imgproc.circle(withDarts, centerOfDartboardWithDarts, 3, new Scalar(255, 255, 0), -1);
//      HighGui.imshow("test image 2 ", withDarts);

      //centered sub images
      Rect rectD = new Rect((int) centerOfDartboard.x - 200, (int) centerOfDartboard.y - 200, 400, 400);
      Mat subImage_dartboard = new Mat(dartboard, rectD);

      Rect rectDWD = new Rect((int) centerOfDartboardWithDarts.x - 200, (int) centerOfDartboardWithDarts.y - 200, 400, 400);
      Mat subImage_dartboardWithDarts = new Mat(withDarts, rectDWD);

     // HighGui.imshow("centered dartboard", subImage_dartboard);
     // HighGui.imshow("centered dartboard with darts", subImage_dartboardWithDarts);

      Mat diffImageWithoutBlur = new Mat();
      Core.absdiff(subImage_dartboard, subImage_dartboardWithDarts, diffImageWithoutBlur);

      if (DEBUG) {
        HighGui.imshow("(0) diffImage raw", diffImageWithoutBlur);
      }
     Mat pointsAtSubImage = subImage_dartboard.clone();
     Mat pointsAtBoard = withDarts.clone();
      Point[] nonCenteredDarts = findDartTips(diffImageWithoutBlur);
      //HighGui.waitKey();


      int i = 0;
      for (Point p : nonCenteredDarts
      ) {
        Imgproc.circle(pointsAtSubImage, p, 3, new Scalar(255, 0, 255), -1);

        //transform coordinates to global
        Point tmp = new Point(centerOfDartboardWithDarts.x + p.x - 200, centerOfDartboardWithDarts.y + p.y - 200);
        result[i] = tmp;
        //System.out.print(tmp);
        i++;
        Imgproc.circle(pointsAtBoard, tmp, 3, new Scalar(255, 0, 255), -1);
      }
      //HighGui.imshow("Points on SubImage", pointsAtSubImage);
      //HighGui.imshow("Points on Dartboard", pointsAtBoard);


//       paintCircels(dartboard, circles, false);
//       Imgproc.circle(dartboard, centerOfDartboard, 3, new Scalar(255, 255, 0), -1);
//      paintCircels(withDarts, circles1, false);
//      paintCircels(withDarts, circles2, false);
//      Imgproc.circle(withDarts, centerOfDartboardWithDarts, 3, new Scalar(255, 255, 0), -1);
//
//      HighGui.namedWindow("test image 3", HighGui.WINDOW_AUTOSIZE);
//      HighGui.namedWindow("test image 4", HighGui.WINDOW_NORMAL);
//      HighGui.imshow("test image 3", edges);
//      HighGui.imshow("test image 4", dartboard);
//      HighGui.imshow("test image 5", withDarts);

      //HighGui.waitKey();

      //resize to origin
      Mat help = withDarts.clone();
      threshold(help, help, 0, 0, 1);
      for (Point p : result
      ) {
        Imgproc.circle(help, p, 0, new Scalar(255, 255, 255), -1);
      }

      resize(help, help, new Size(x, y));
      //HighGui.imshow("test", help);
      //HighGui.waitKey();
      ArrayList<Point> finalPoints=new ArrayList<>();
      int a = 0;
      for (int yi = 0; yi < help.rows(); yi++
      ) {
        for (int xi = 0; xi < help.cols(); xi++) {
          if (help.get(yi, xi)[0] > 150) {
            if (a > 0 && ((Math.abs(finalPoints.get(a - 1).y - yi) > 5) || (Math.abs(finalPoints.get(a - 1).x - xi) > 5))) {
              finalPoints.add( new Point(xi, yi));
              a++;
            } else if(a==0) {
              finalPoints.add( new Point(xi, yi));
              a++;
            }
          }
        }
      }

      return finalPoints.toArray(new Point[]{});

    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

  private Point[] findDartTips(Mat diffImage) {

    Point[] result = new Point[3];
    Mat gray = new Mat();
    Mat blackAndWhite = new Mat();
    Imgproc.cvtColor(diffImage, gray, Imgproc.COLOR_RGB2GRAY);
    double thresh = 4;
    Imgproc.threshold(gray, blackAndWhite, thresh, 255.0, THRESH_BINARY);
    Mat edges = edgeDetection(blackAndWhite);

    if (DEBUG) {
      HighGui.imshow("(1) diffImage binary", blackAndWhite);
      HighGui.imshow("(2) diffImage edges", edges);
    }


    Mat tmp = new Mat();
    Imgproc.cvtColor(blackAndWhite, tmp, THRESH_BINARY_INV);
    Mat imgWithMiddleLine = drawMiddleLine(blackAndWhite, false);
    if (DEBUG) {
      HighGui.imshow("(3) image with middle line", imgWithMiddleLine);
    }
    Mat grey = new Mat();
    Imgproc.cvtColor(imgWithMiddleLine, grey, COLOR_RGB2GRAY);

   // HighGui.imshow("grey lines", grey);
    Mat binary = new Mat();
    Imgproc.threshold(grey, binary, 50, 255.0, THRESH_BINARY);

    Mat tmpImg = blackAndWhite.clone();
    threshold(tmpImg, tmpImg, 255, 0, 1);
    Imgproc.cvtColor(tmpImg, tmpImg, COLOR_GRAY2RGB);


    Mat lines = new Mat();
    Imgproc.HoughLinesP(binary, lines, 1, Math.PI / 180, 8, 2, 3);

    for (int i = 0; i < lines.rows(); i++) {
      double[] val = lines.get(i, 0);
      Imgproc.line(tmpImg, new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(0, 0, 255), 1);
    }

   // HighGui.imshow("tmpImg", tmpImg);
    Mat linesP = new Mat();
    Imgproc.cvtColor(tmpImg, linesP, COLOR_RGB2GRAY);
    Mat resultImg = edges.clone();
    Imgproc.cvtColor(resultImg, resultImg, COLOR_GRAY2RGB);
    Mat lines2 = new Mat();
    Imgproc.HoughLines(linesP, lines2, 1, Math.PI / 180, 8);

    Mat intersectionPoints = edges.clone();
    Imgproc.cvtColor(intersectionPoints, intersectionPoints, COLOR_GRAY2RGB);
    ArrayList<Point> firstClassPoints = new ArrayList<>();
    ArrayList<Point> secondClassPoints = new ArrayList<>();
    ArrayList<Point> thirdClassPoints = new ArrayList<>();

    for (int x = 0; x < lines2.rows(); x++) {
      double data[] = lines2.get(x, 0);
      double rho1 = data[0];
      double theta1 = data[1];
      double cosTheta = Math.cos(theta1);
      double sinTheta = Math.sin(theta1);
      double x0 = cosTheta * rho1;
      double y0 = sinTheta * rho1;

      if (sinTheta >= 0.64) {
        //draw the line in resultImg
        Point pt1 = new Point(x0 - 100 * (-sinTheta), y0 - 100 * cosTheta);
        Point pt2 = new Point(x0 - 425 * (-sinTheta), y0 - 425 * cosTheta);
        Imgproc.line(resultImg, pt1, pt2, new Scalar(0, 0, 255), 1);

        //goes along the line from bottom to top and stop at first white point/edge
        for (int i = 425; i >= 100; i--) {
          Point help = new Point(x0 - i * (-sinTheta), y0 - i * cosTheta);
          double[] pxl = edges.get((int) help.y, (int) (help.x));
          if (pxl != null && pxl[0] > 0) {
            Imgproc.circle(intersectionPoints, help, 1, new Scalar(0, 0, 255));
            firstClassPoints.add(help);
            break;
          }
        }

      } else if (sinTheta > 0.35) {
        //draw the line in resultImg
        Point pt1 = new Point(x0 - 100 * (-sinTheta), y0 - 100 * cosTheta);
        Point pt2 = new Point(x0 - 400 * (-sinTheta), y0 - 400 * cosTheta);
        Imgproc.line(resultImg, pt1, pt2, new Scalar(0, 255, 0), 1);
        //goes along the line from bottom to top and stop at first white point/edge
        for (int i = 400; i >= 100; i--) {
          Point help = new Point(x0 - i * (-sinTheta), y0 - i * cosTheta);
          double[] pxl = edges.get((int) help.y, (int) (help.x));
          if (pxl != null && pxl[0] > 0) {
            Imgproc.circle(intersectionPoints, help, 1, new Scalar(0, 255, 0));
            secondClassPoints.add(help);
            break;
          }
        }
      } else if (sinTheta > 0.1 && sinTheta <= 0.35) {
        //draw the line in resultImg
        Point pt1 = new Point(x0 - 50 * (-sinTheta), y0 - 50 * cosTheta);
        Point pt2 = new Point(x0 - 400 * (-sinTheta), y0 - 400 * cosTheta);
        Imgproc.line(resultImg, pt1, pt2, new Scalar(255, 0, 0), 1);
        //goes along the line from bottom to top and stop at first white point/edge
        for (int i = 400; i >= 50; i--) {
          Point help = new Point(x0 - i * (-sinTheta), y0 - i * cosTheta);
          double[] pxl = edges.get((int) help.y, (int) (help.x));
          if (pxl != null && pxl[0] > 0) {
            Imgproc.circle(intersectionPoints, help, 1, new Scalar(255, 0, 0));
            thirdClassPoints.add(help);
            break;
          }
        }
      }
    }
    if (DEBUG) {
      HighGui.imshow("(4) founded lines", resultImg);
    }
   // HighGui.imshow("intersection points", intersectionPoints);

    //FIXME Problem bei Darts die in einer Linie sind funktioniert das von unten hochlaufen = dart spitze nicht mehr.
    // Bzw. Teile der Linien des einen Darts schneiden einen anderen Dart z.B. am Flight.
    // Weshalb mehr Schnittpunkte erkannt werden als eig soll.
    // Gelöst habe ich das durch das Erhöhen der nearBy-Werte.
    // Allerdings könnte es dadurch zu Problemen bei sehr nahen Darts kommen.

    ArrayList<Point> resultDartPoints = new ArrayList<Point>();
    int nearByX = 10;
    int nearByY = 30;

    for (Point secondP :
      firstClassPoints) {
      boolean collision = false;
      for (Point firstP : resultDartPoints) {
        if ((secondP.x > firstP.x - nearByX && secondP.x < firstP.x + nearByX) && (secondP.y > firstP.y - nearByY && secondP.y < firstP.y + nearByY)) {
          collision = true;
        }
      }
      if (!collision) {
        resultDartPoints.add(secondP.clone());
      }
    }
    if (resultDartPoints.size() < 3) {
      for (Point secondP :
        secondClassPoints) {
        boolean collision = false;
        for (Point firstP : resultDartPoints) {
          if ((secondP.x > firstP.x - nearByX && secondP.x < firstP.x + nearByX) && (secondP.y > firstP.y - nearByY && secondP.y < firstP.y + nearByY)) {
            collision = true;
          }
        }
        if (!collision) {
          resultDartPoints.add(secondP.clone());
        }
      }
    }
    if (resultDartPoints.size() < 3) {
      for (Point secondP :
        thirdClassPoints) {
        boolean collision = false;
        for (Point firstP : resultDartPoints) {
          if ((secondP.x > firstP.x - nearByX && secondP.x < firstP.x + nearByX) && (secondP.y > firstP.y - nearByY && secondP.y < firstP.y + nearByY)) {
            collision = true;
          }
        }
        if (!collision) {
          resultDartPoints.add(secondP.clone());
        }
      }
    }

    //create result
    if (resultDartPoints.size() == 3) {
      Mat ret = edges.clone();
      Imgproc.cvtColor(ret, ret, COLOR_GRAY2RGB);
      int i = 0;
      for (Point point : resultDartPoints
      ) {
        result[i] = point.clone();
        Imgproc.circle(ret, point, 1, new Scalar(255, 255, 128), 3);
        i++;
      }
      if (DEBUG) {

        HighGui.imshow("(5) DartPoints", ret);
      }
    } else {
      Mat error = edges.clone();
      Imgproc.cvtColor(error, error, COLOR_GRAY2RGB);
      for (Point point : resultDartPoints
      ) {
        Imgproc.circle(error, point, 1, new Scalar(255, 255, 128), 3);
      }
      //siehe fix-me (oben)

      if (DEBUG) {
        HighGui.imshow("(5) wrong amount of DartPoints, please find error", error);
      }
    }

    return result;
  }

  double[] getDistanceToNearEdges(Point point, Mat allEdges) {
    // Imgproc.circle(allEdges, point, 2, new Scalar(255, 255, 0), -1);
    Mat result = allEdges.clone();
    cvtColor(result, result, COLOR_GRAY2RGB);
    double i = 0.0;
    double[] res = new double[4];

    while (true) {
      double[] tmp = allEdges.get((int) point.y, (int) (point.x - i));
      if (tmp[0] > 0) {
        Point foundedEdge = new Point((point.x - i), point.y);
//        System.out.println("lefter edge :  I=" + i + " founded edge at" + foundedEdge + "with value " + tmp[0]);
        Imgproc.circle(result, foundedEdge, 1, new Scalar(255, 255, 0), -1);
        res[0] = i;
        break;
      } else {
        i++;
      }
    }
    i = 0.0;
    while (true) {
      double[] tmp = allEdges.get((int) point.y, (int) (point.x + i));
      if (tmp[0] > 0) {
        Point foundedEdge = new Point((point.x + i), point.y);
//        System.out.println("right edge:  I=" + i + " founded edge at" + foundedEdge + "with value " + tmp[0]);
        res[1] = i;
        Imgproc.circle(result, foundedEdge, 1, new Scalar(255, 255, 0), -1);
        break;
      } else {
//        System.out.println("x=" + (point.x + i) + " y=" + point.y + " " + tmp[0]);
        i++;
      }
    }
    i = 0.0;
    while (true) {
      double[] tmp = allEdges.get((int) (point.y - i), (int) point.x);
      if (tmp[0] > 0) {
        Point foundedEdge = new Point(point.x, (point.y - i));
//        System.out.println("upper edge :  I=" + i + " founded edge at" + foundedEdge + "with value " + tmp[0]);
        Imgproc.circle(result, foundedEdge, 1, new Scalar(255, 255, 0), -1);
        res[2] = i;
        break;
      } else {
        i++;
      }
    }
    i = 0.0;
    while (true) {
      double[] tmp = allEdges.get((int) (point.y + i), (int) point.x);
      if (tmp[0] > 0) {
        Point foundedEdge = new Point(point.x, (point.y + i));
//        System.out.println("downer edge :  I=" + i + " founded edge at" + foundedEdge + "with value " + tmp[0]);
        Imgproc.circle(result, foundedEdge, 1, new Scalar(255, 255, 0), -1);
        res[3] = i;
        break;
      } else {
        i++;
      }
    }

    //current center
    Imgproc.circle(result, point, 1, new Scalar(255, 0, 255), -1);
//    resize(result, result, new Size(xSize * 4, ySize * 4));
//    HighGui.imshow("edges " + point, result);
    return res;
  }

  private Mat drawMiddleLine(Mat imageBlackWhite, boolean withEdges) {
//    Mat ret = new Mat(400, 400, 0);
    //Mat(imageBlackWhite.rows(),imageBlackWhite.cols(),0);
    Mat ret = imageBlackWhite.clone();
    threshold(ret, ret, 0, 10, 1);
    Mat edges = edgeDetection(imageBlackWhite);
    cvtColor(edges, edges, COLOR_GRAY2RGB);
    cvtColor(ret, ret, COLOR_GRAY2RGB);
    for (int y = 0; y < imageBlackWhite.rows(); y++) {
      for (int x = 0; x < imageBlackWhite.cols(); x++) {
        double[] tmp = imageBlackWhite.get(y, x);
        if (tmp != null && tmp[0] > 0) {
          int xStart = x;
          while (tmp != null && tmp[0] > 0) {
            x++;
            tmp = imageBlackWhite.get(y, x);
          }
          int xEnd = x;
          if (xEnd - xStart > 5 && xEnd - xStart < 10) {
            Point draw = new Point(xStart + ((xEnd - xStart) / 2), y);
            Imgproc.circle(edges, draw, 0, new Scalar(255, 255, 0), 1);
            Imgproc.circle(ret, draw, 0, new Scalar(255, 255, 0), 1);
          }
        }
      }
    }
    if (withEdges) {
      return edges;
    } else {
      return ret;
    }
  }
//  private void skeleton(Mat _img) {
//    boolean done = false;
//    Mat img = _img.clone();
//    HighGui.imshow("tresh_1", img);
//
//    Mat imgGray = new Mat();
//    Imgproc.cvtColor(img, imgGray, Imgproc.COLOR_BGR2GRAY);
//
//    Mat tresh = new Mat();
//    double thresh = Imgproc.threshold(imgGray, tresh, 100, 255, Imgproc.THRESH_BINARY_INV | Imgproc.THRESH_OTSU);
//    HighGui.imshow("tresh_1", tresh);
//
//    Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, new Size(3, 3));
//    Mat eroded = new Mat();
//    Mat temp = new Mat();
//    Mat skel = new Mat(tresh.rows(), tresh.cols(), CvType.CV_8UC1, new Scalar(0));
//
//    int size = _img.cols() * _img.rows();
//    int zeros = 0;
//
//    while (!done) {
//      Imgproc.erode(tresh, eroded, element);
//      Imgproc.dilate(eroded, temp, element);
//      Core.subtract(tresh, temp, temp);
//      Core.bitwise_or(skel, temp, skel);
//      eroded.copyTo(tresh);
//
//      zeros = size - Core.countNonZero(tresh);
//      if (zeros == size)
//        done = true;
//    }
//    HighGui.imshow("Skeleton", skel);
//  }

  public static Point getAverageCenterOfAll(Mat[] circles) {
    int counter = 0;
    double sumX = 0;
    double sumY = 0;

    for (int j = 0; j < circles.length; j++) {
      for (int i = 0; i < circles[j].cols(); i++) {
        double[] data = circles[j].get(0, i);
        sumX += data[0];
        sumY += data[1];
        counter++;
      }
    }
    return new Point(sumX / counter, sumY / counter);
  }

  static Mat edgeDetection(Mat image) {
    Mat edges = new Mat();
    Imgproc.Canny(image, edges, MAX_LOW_THRESHOLD, MAX_LOW_THRESHOLD * RATIO, KERNEL_SIZE, false);

    return edges;
  }

  static Mat circleDetection(Mat edges, int variant) {
    Mat circles = new Mat();
    if (variant == 1) {
      Imgproc.HoughCircles(edges, circles, Imgproc.CV_HOUGH_GRADIENT, 1, 100, MAX_LOW_THRESHOLD * RATIO, MAX_LOW_THRESHOLD, 0, 0);
    } else if (variant == 2) {
      Imgproc.HoughCircles(edges, circles, Imgproc.CV_HOUGH_GRADIENT, 1, 100, 1500, 150, 0, 0);
    } else if (variant == 3) {
      Imgproc.HoughCircles(edges, circles, Imgproc.CV_HOUGH_GRADIENT, 1, 35, 700, 140, 0, 0);
    }
    return circles;
  }

  static void paintCircels(Mat color, Mat circles, boolean withCenter) {
    double x = 0.0;
    double y = 0.0;
    int r = 0;

    for (int i = 0; i < circles.cols(); i++) {
      double[] data = circles.get(0, i);
      for (int j = 0; j < data.length; j++) {
        x = data[0];
        y = data[1];
        r = (int) data[2];
      }
      Point center = new Point(x, y);

      if (withCenter) {
        // circle center
        Imgproc.circle(color, center, 3, new Scalar(0, 255, 0), -1);
      }
      // circle outline
      Imgproc.circle(color, center, r, new Scalar(255, 0, 0), 1);
    }
  }
}
