package git.rwth_aachen.de.data_analysis;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.nio.file.Path;

import static git.rwth_aachen.de.data_analysis.Main.xSize;
import static git.rwth_aachen.de.data_analysis.Main.ySize;

public class BoardDetection {
  private static final int MAX_LOW_THRESHOLD = 50;
  private static final int RATIO = 3;
  private static final int KERNEL_SIZE = 3;

  private Path boardPath;

  private Point center;
  private int radius;

  public BoardDetection(Path board) {
    this.boardPath = board;
  }

  public boolean init() {
    Mat img = Imgcodecs
      .imread(boardPath.toString(), Imgcodecs.IMREAD_GRAYSCALE);
    Mat imgColor = Imgcodecs
      .imread(boardPath.toString(), Imgcodecs.IMREAD_COLOR);




    Mat onlyRed = filterColor(imgColor, Color.RED, 15);
    Mat onlyGreen = filterColor(imgColor, Color.GREEN, 15);

    Mat imgColorless = new Mat();
    Core.bitwise_or(onlyRed, onlyGreen, imgColorless);

    Mat imgDotLess = removeDots(imgColorless, 15);

    Mat edges = edgeDetection(imgDotLess);
    Mat circles = circleDetection(edges);

    if (circles.cols() == 0) {
      System.err.println("No Circles Detected!");
      return false;
    }

    double[] middlePoint = circles.get(0, 0);
    center = new Point(middlePoint[0], middlePoint[1]);
    radius = (int) middlePoint[2];
    return true;
  }

  public int calculatePoints(Point staticPoint) {
    return pixelToPoints(center, radius, staticPoint);
  }

  public int getRadius() {
    return radius;
  }

  public Point getCenter() {
    return center;
  }

  private static Mat edgeDetection(Mat image) {
    Mat edges = new Mat();
    Imgproc.Canny(image, edges, MAX_LOW_THRESHOLD, MAX_LOW_THRESHOLD * RATIO, KERNEL_SIZE, false);
    return edges;
  }

  private static Mat circleDetection(Mat edges) {
    Mat circles = new Mat();
    Imgproc.HoughCircles(edges, circles, Imgproc.CV_HOUGH_GRADIENT, 1, (double) edges.rows() / 2, MAX_LOW_THRESHOLD * RATIO, MAX_LOW_THRESHOLD, 0, 0);
    return circles;
  }

  private static Mat filterColor(Mat image, Color color, int sensitivity) {
    Mat hsvImage = new Mat();
    Imgproc.cvtColor(image, hsvImage, Imgproc.COLOR_BGR2HSV);

    Mat filtered = new Mat();
    switch (color) {
      case RED:
        // roter Bereich am linken Ende des HSV Spektrums
        Mat mask0 = new Mat();
        Scalar lower_0 = new Scalar(0, 100, 100);
        Scalar upper_0 = new Scalar(sensitivity, 255, 255);
        Core.inRange(hsvImage, lower_0, upper_0, mask0);
        // roter Bereich am rechten Ende des HSV Spektrums
        Mat mask1 = new Mat();
        Scalar lower_1 = new Scalar(180 - sensitivity, 100, 100);
        Scalar upper_1 = new Scalar(180, 255, 255);
        Core.inRange(hsvImage, lower_1, upper_1, mask1);
        Core.bitwise_or(mask0, mask1, filtered);
        break;
      case GREEN:
        lower_0 = new Scalar(35, 0, 0);
        upper_0 = new Scalar(85, 255, 255);
        Core.inRange(hsvImage, lower_0, upper_0, filtered);
        break;
    }

    return filtered;
  }

  private static Mat removeDots(Mat image, int dotSize) {
    Mat dotLess = new Mat();
    Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(dotSize, dotSize));
    Imgproc.morphologyEx(image, dotLess, Imgproc.MORPH_OPEN, kernel);

    return dotLess;
  }

  private static int pixelToPoints(Point center, int radius, Point dot) {
    if(center == null || radius < 1) {
      throw new IllegalArgumentException();
    }

    double relativeX = dot.x - center.x;
    double relativeY = dot.y - center.y;

    double m = relativeY / relativeX;

    double degree = Math.toDegrees(Math.atan(m));
    double distance = Math.sqrt(Math.pow(relativeX,2) + Math.pow(relativeY,2));


    if (relativeX >= 0 && relativeY >= 0) {
      degree = 360 - degree;
    } else if (relativeX == 0 && relativeY >= 0) {
      degree = 270;
    } else if (relativeX < 0 && relativeY >= 0) {
      degree = Math.abs(degree) + 180;
    } else if (relativeX <= 0 && relativeY < 0) {
      degree = 180 - degree;
    } else if (relativeX > 0 && relativeY < 0) {
      degree = Math.abs(degree);
    }

    int points = 0;

    int value = (int) (degree / 9);

    switch (value) {
      case 0:
      case 39:
      case 40:
        points = 6;
        break;
      case 1:
      case 2:
        points = 13;
        break;
      case 3:
      case 4:
        points = 4;
        break;
      case 5:
      case 6:
        points = 18;
        break;
      case 7:
      case 8:
        points = 1;
        break;
      case 9:
      case 10:
        points = 20;
        break;
      case 11:
      case 12:
        points = 5;
        break;
      case 13:
      case 14:
        points = 12;
        break;
      case 15:
      case 16:
        points = 9;
        break;
      case 17:
      case 18:
        points = 14;
        break;
      case 19:
      case 20:
        points = 11;
        break;
      case 21:
      case 22:
        points = 8;
        break;
      case 23:
      case 24:
        points = 16;
        break;
      case 25:
      case 26:
        points = 7;
        break;
      case 27:
      case 28:
        points = 19;
        break;
      case 29:
      case 30:
        points = 3;
        break;
      case 31:
      case 32:
        points = 17;
        break;
      case 33:
      case 34:
        points = 2;
        break;
      case 35:
      case 36:
        points = 15;
        break;
      case 37:
      case 38:
        points = 10;
        break;
      default:
        return -1;
    }

    double bullsEyeDist = (12.7 / (340-16)) * radius;
    double bullDist = (31.8 / (340-16)) * radius;
    double tripleMin = (99.0 / (170-8)) * radius;
    double tripleMax = (107.0 / (170-8)) * radius;
    double doubleMin = (162.0 / (170-8)) * radius;
    double doubleMax = (170.0 / (170-8)) * radius;

    if (distance <= bullsEyeDist) {
      return 50;
    }

    if (distance <= bullDist) {
      return 25;
    }

    if (distance >= tripleMin && distance <= tripleMax) {
      points *= 3;
      return points;
    }

    if (distance >= doubleMin && distance <= doubleMax) {
      points *= 2;
      return points;
    }

    if (distance > radius) {
      return 0;
    }

    return points;
  }
}
